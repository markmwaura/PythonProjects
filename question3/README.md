# Django Feedback App

  This project contains  a simple web app that allows a user to login/signup and fill out a feedback form.
  The feedback datais only viewed by an admin/superuser who can then export the data into a csv.the required fields are:
  *Name(automatically loaded from user object)*
  *Phone Number(validated)*
  *Neighbourhood - A drop down with a list of hoods*
  *Rating - Choice fields from 1 to 5*
  *Comments - text area field*

# Dependencies
  This project uses :
  *Django 1.8.12*
  *Postgresql 9.5*
  *Twitter bootstrap v3(css and javascript)*

# Installation
    Clone the entire repository folder to you machine
    Change directory to inside question3 folder and create your virtual 
    environment i.e pip install virtualenv venv
    
    -Activate it source venv/bin/activate
    cd usersproject
    pip install -r requirements.txt
    python manage.py migrate
    python manage.py runserver 










 
