from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib import auth
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.contrib.auth.forms import UserCreationForm


def index(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('index.html', c)


def login(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('login.html', c)


def auth_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')

    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('loggedin')
    else:
        return HttpResponseRedirect('invalid')


def loggedin(request):

    # query_results = feedback.objects.all()
    return render_to_response('loggedin.html', {'user':request.user,'full_name': request.user.username})

def invalid_login(request):
    return render_to_response('invalid_login.html')

def logout(request):
    auth.logout(request)
    # Shortcut level method, with fail_silently=True
    # messages.info(request, 'All items on this page have free shipping.', fail_silently=True)
    #messages.info(request, 'Your password has been changed successfully!')
    return render_to_response('logout.html',{'messages':'Your are now logged out!'})


def register(request):

    if request.method =='POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('register_success')


    args = {}
    args.update(csrf(request))
    args['form'] = UserCreationForm()
    return render_to_response('register.html',args)


def register_success(request):
    return render_to_response('register_success.html')

