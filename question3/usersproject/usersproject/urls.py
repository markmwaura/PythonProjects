"""usersproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

# Login
    url(r'^accounts/login/$', 'usersproject.views.login', name='login'),
    url(r'^accounts/auth/loggedin/$', 'usersproject.views.loggedin',name='loggedin'),
    url(r'^accounts/auth/invalid/$', 'usersproject.views.invalid_login', name='invalid'),
    url(r'^accounts/auth/$', 'usersproject.views.auth_view'),
    url(r'^accounts/logout/$', 'usersproject.views.logout'),
    url(r'^userfeedback/form_data/$',
        'userfeedback.views.post_form_upload', name='post_form_upload'),
    # Register
    url(r'^accounts/register/$', 'usersproject.views.register', name='register'),
    url(r'^accounts/register/register_success/$', 'usersproject.views.register_success',name='register_success'),
    url(r'^$', 'usersproject.views.index', name='index'),
#     Feedback

    # url(r'^feedback/$', 'userfeedback.views.feedbackp'),
    # url(r'^feedback/index/$', 'userfeedback.views.index', name='index'),
    # url(r'^feedback/feedback/success/$', 'userfeedback.views.feedbackp', name='feedback_success'),
]
