from django import forms

class FeedbackForm(forms.Form):
	name = forms.CharField(max_length=254)
	phonenumber = forms.RegexField(regex=r'^\+?1?\d{9,13}$')
	neighbourhood = forms.CharField()
	rating = forms.CharField()
	comments = forms.CharField()