from __future__ import unicode_literals

from django.db import models

# Create your models here.
LOCATIONS = (
    ('NRB', 'Nairobi'),
    ('KIS', 'Kisumu'),
    ('MBS', 'Mombasa'),
    ('MACH', 'Machakos'),
    ('KIT', 'Kitui'),)
CHOICES = (('1','1'),
               ('2','2'),
               ('3','3'),
               ('4','4'),)

class Userfeedback(models.Model):
    name = models.CharField(max_length=200)
    phonenumber = models.CharField(max_length=200)
    neighbourhood= models.CharField(max_length=200)
    rating = models.CharField(max_length=200)
    comments = models.CharField(max_length=200)

    class Meta:
        verbose_name_plural = "Userfeedback"

        def __str__(self):
            return self.name


  