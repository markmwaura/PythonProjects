from django.contrib import admin
from userfeedback.models import Userfeedback

# Register your models here.
admin.site.register(Userfeedback)
