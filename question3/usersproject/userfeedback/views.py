from django.shortcuts import render
from userfeedback.models import Userfeedback
from django.http import HttpResponseRedirect

# Create your views here.
from userfeedback.forms import FeedbackForm
 
def post_form_upload(request):
    if request.method == 'GET':
        form = FeedbackForm()
        user = request.user
    else:
        # A POST request: Handle Form Upload
        form = FeedbackForm(request.POST) # Bind data from request.POST into a FeedbackForm
 
        # If data is valid, proceeds to create a new post and redirect the user
        if form.is_valid():
            name = form.cleaned_data['name']
            phonenumber = form.cleaned_data['phonenumber']
            neighbourhood = form.cleaned_data['neighbourhood']
            rating = form.cleaned_data['rating']
            comments = form.cleaned_data['comments']
            userfeedbck = Userfeedback(name=name,phonenumber=phonenumber,
            	neighbourhood=neighbourhood,rating=rating,comments=comments)

            userfeedbck.save()

            return HttpResponseRedirect('.')
 
    return render(request, 'feedback.html', {
        'form': form,
        'user':user,
        
    })