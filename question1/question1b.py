import csv
from datetime import datetime


def harvestsort():
    with open("Sample Data for test.csv", "r") as infile, open("Sample Data by date in ASC order.csv", "w") as outfile:
        reader = csv.reader(infile)
        harvestlist = []
        next(reader, None)  # skip the headers

        readdates = sorted(reader, key=lambda row: datetime.strptime(row[7], '%m/%d/%Y'))

        # print(datar)

        #
        writer = csv.writer(outfile)
        for row in readdates:
            # process each row
            writer.writerow(row)

            # no need to close, the files are closed automatically


if __name__ == '__main__':
    harvestsort()
