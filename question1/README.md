# Farm App
    This is a simple farm management app that shows:
    *Farmers with the highest and loweest biomass*
    *Sort the list given in the excel file in ascending order by
    harvesting date*
    *Finds the average number of plants harvested*

# Run it
    To see the app in action place the excel sheet "Sample Data for test.xlsx"
    in the same directory as the python file.
    For section 2 (sorting) - you need to save the original "Sample data for test.xlsx" as a csv file i.e .csv
