import openpyxl
from datetime import datetime


def farmers():
    wb = openpyxl.load_workbook('Sample Data for test.xlsx')
    sheet = wb.get_sheet_by_name('Sheet1')

    # Get Column sections on sheet
    farmernames = sheet.columns[0]
    totalbiomass = sheet.columns[11]
    harvestdates = sheet.columns[8]

    # initialize empty list for maximum biomas and farmer list
    maxlist = []
    namelist = []


    for totalbio in totalbiomass[1:]:
        maxlist.append(totalbio.value)
    #print(max(maxlist), min(maxlist))  # print maximum and minimum item in maxlist  # Find index of largest value in list
    largevalueindex = maxlist.index(max(maxlist))
    # Find index of smallest vaue in list
    smallvalueindex = maxlist.index(min(maxlist))

# populate namelist list with farmer names
    for farmername in farmernames[1:]:
        namelist.append(farmername.value)

    print('Farmer with highest biomass is' + namelist[largevalueindex], 'whose biomass is',
          max(maxlist), 'Kgs')

    print('Farmer with lowest biomass is ' + namelist[smallvalueindex], 'whose biomass is',
          min(maxlist), 'kgs')

farmers()
