# Python Projects
    This repo contains question1 and question3 which are different projects 

### Question1

    The folder in question1 consists of applications written in pure python
    that read data from and excel page and performs the following analysis

    *Find the farmers with the highest and lowest Total biomass Fwt (kg)*
    *Sorts the list in ascending order by Harvesting date*
    *Find the average No of plants harvest*


### Question3

    This folder contains a simple web app written in django that allows a user to login/signup and fill out a feedback form.
    The feedback data is only viewed by an admin/superuser who can then export the data to into a csv.
        This project uses:
        *Django 1.8.12
        *Postgresql 9.5
        *Twitter boostrap v3